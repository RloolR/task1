# Repositório para o Desafio Zenvia

## Arquivos desta tarefa:
- [x] cv-deployment.yaml
- [x] nginx.conf
- [x] Dockerfile

## Observações
Kubernetes ainda é uma tecnologia que irei me dedicar a aprender, desenvolvi a tarefa baseado no pouco que conheço até o momento.

Cheguei a cogitar a criar algo no EKS mas não identifiquei na descrição da tarefa algo que me desse esta opção e então segui com o entendimento de que era apenas para criar os arquivos de referencias.

### 3a parte
