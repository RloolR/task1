FROM nginx:alpine
MAINTAINER Rafael Oliveira <rafael.oliveira.sof2web@gmail.com>

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

RUN echo "daemon off;" >> /etc/nginx/nginx.conf \
 && sed -i 's/worker_processes  1/worker_processes  auto/' /etc/nginx/nginx.conf

COPY . /app/
WORKDIR /app/